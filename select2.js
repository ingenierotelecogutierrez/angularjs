/**
 * Created by Jesus on 25/01/2017.
 */

angular
    .module('acumulador', [])
    .controller('acumuladorAppCtrl', controlMain);

function controlMain() {
    this.total = 0;
    this.cuanto = 1;

    this.tamTitular = "titularpeq";

    this.sumar = function () {
        this.total += parseInt(this.cuanto);
    }

    this.restar = function () {
        this.total -= parseInt(this.cuanto);
    }

    this.clases = ["un", "dos", "tres"];
}