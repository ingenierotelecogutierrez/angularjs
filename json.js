/**
 * Created by JMG on 29/01/2017.
 */


angular
    .module('jsonApp', [])
    .controller('jsonAppCtrl', ['$http', mainCtrl]);

function mainCtrl($http) {
    var vm = this;

    vm.buscaCiudad = function () {
        var url = "http://localhost/angularjs/library/country_codes.json?callback=JSON_CALLBACK";

        $http.get(url).then(function (respuesta) {
            //console.log("res:", respuesta.data);
            vm.countries = respuesta.data;
        });
    }
}