/**
 * Created by Jesus on 25/01/2017.
 */

angular
    .module('repeatApp')
    .controller('repeatAppCtrl', controlMain);

function controlMain() {
    this.orden = false;
    this.campo = "name";

    var url = "http://api.openbeerdatabase.com/v1/beers.json?callback=JSON_CALLBACK";

    if (this.name){
        url += "&query=" + this.nombre;
    }

    this.buscaCervezas = function () {
        $http.jsonp(url).success(function(respuesta){
            console.log("res:", respuesta);
            this.cervezas = respuesta.beers;
        });
    }

}