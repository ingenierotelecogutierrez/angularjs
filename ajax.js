/**
 * Created by JMG on 28/01/2017.
 */


angular
    .module('ajaxRest',[])
    .controller('ajaxRestCtrl', ['$http', ctrlMain]);

function ctrlMain($http) {
    var vm = this;

    vm.buscaRegion = function () {
        $http.get(vm.url)
            .then(function (respuesta) {
                //console.log("res:", respuesta);
                vm.paises = respuesta.data;
        });
    }

}