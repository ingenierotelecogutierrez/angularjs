/**
 * Created by Jesus on 19/01/2017.
 */

angular
    .module('acumuladorApp', [])
    .controller('acumuladorAppCtrl', controlPrincipal);

function controlPrincipal() {
    this.total = 0;
    this.cuanto = 0;

    this.sumar = function () {
        this.total += parseInt(this.cuanto);
    }
    this.restar = function () {
        this.total -= parseInt(this.cuanto);
    }
}